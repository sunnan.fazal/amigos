<?php 
  @include("template.php"); 
    require("conn.php");
    $sql = "SELECT * from gallery";
    $query = $conn->query($sql);
    ?>
?>
<div class="sections">
    <div class="container">
        <div class="pages-title">
            <h1>AMIGOS <br> <span>Gallery</span></h1>
            <p><a href="index.php">Home</a> &nbsp; > &nbsp; <a href="gallery.php">Gallery</a></p>
        </div>
    </div>  
</div>
    <section>
        <div class="container">
			<div class="row">
              <div class="col-sm-12">
                <div class="section-tittle-alt">
		            <h5>OUR PROUD</h5>
		            <h2>Moments</h2>
		        </div>  
              </div>
            </div>
            
		<div class="row hover-effects image-hover">
      <?php
        while($row = $query->fetch_assoc())
        {
        ?>
        <div class="col-md-4 col-lg-4">
          <div class="service-box">
              <figure class="service-thumbnail"><img src="<?= "admin/".$row["IMAGE"] ?>" style="height: 13.35em;" alt=""></figure> 
          </div>
        </div>
        <?php        
        }
      ?>
		</div>
        </div>
    </section>
<?php 
  @include("footer.php");
?>