<link rel="shortcut icon" href="img/master/favicon.png">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="fonts/css/all.min.css">
<link rel="stylesheet" href="css/navigation.css">
<link rel="stylesheet" href="css/stylesheet.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/slick.min.css"> 
<link rel="stylesheet" href="css/owl.carousel.min.css">
<script src="admin/assets/libs/jquery/jquery-1.11.1.min.js"></script>
<script src="js/modernizr-custom.js"></script>

</head>
<body>