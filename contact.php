<?php 
  @include("template.php"); 
?>
<div class="sections">
    <div class="container">
        <div class="pages-title">
            <h1>AMIGOS <br> <span>CONTACT</span></h1>
            <p><a href="index.php">Home</a> &nbsp; > &nbsp; <a href="contact.php">CONTACT US</a></p>
        </div>
    </div>  
</div>

    <!-- CONTENT START -->
    <section>
        <!-- CONTACT INFO START -->  
        <div class="container">
            <div class="section-title">
                <h2>KEEP <span>IN TOUCH</span></h2>
                <p>Feel free to contact us, we are available for your help 24/7</p>
            </div> 
            <div class="row">
              <div class="col-lg-4">
                <div class="contact-box">
                    <figure class="contact-icon"><img src="img/master/telephone.svg" alt=""></figure> 
                    <p>+ 0511 0000 3322</p>  
                </div>
              </div>
              <div class="col-lg-4 center-box">
                <div class="contact-box">
                    <figure class="contact-icon"><img src="img/master/location.svg" alt=""></figure> 
                    <p>8273 NW 56th ST Miami, Florida, 33195 United States</p>  
                </div>
              </div>
              <div class="col-lg-4">
                <div class="contact-box">
                    <figure class="contact-icon"><img src="img/master/close-envelope.svg" alt=""></figure> 
                    <p>support@indusy.com</p>  
                </div>
              </div>
            </div>
        </div>
        <!-- CONTACT INFO END -->

        <!--MAP START-->    
        <div class="container-fluid map-wide">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3300.557832691046!2d72.4576433152185!3d34.18322098057056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0!2zMzTCsDEwJzU5LjYiTiA3MsKwMjcnMzUuNCJF!5e0!3m2!1sen!2s!4v1574493403287!5m2!1sen!2s" class="map-iframe"></iframe>
        </div>
        <!--MAP END--> 

        <!--CONTACT FORM START--> 
        <div class="container">
           <div class="contact-form-3">
                <form id="contact-form">
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input id="form_name" type="text" name="name" class="form-control customize-contact name" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input id="form_email" type="email" name="email" class="form-control customize-contact email" placeholder="Email address">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input id="form_phone" type="tel" name="phone" class="form-control customize-contact phone" placeholder="Please enter your phone">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea id="form_message" name="message" class="form-control customize-contact message" placeholder="Your message" rows="6" required="required" data-error="Please,leave us a message."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p><input type="button" class="btn btn-custom submitForm" value="Send message"></p>
                            </div>
                        </div>
                        <br>
                        <div class="row alertMsg" style="display: none">
                            <div class="col-sm-12">
                                <div class="alert alert-success">Message Sent Successfully !</div>
                            </div>
                        </div>
                    </div>
                </form>  
            </div>
        </div>
        <!--CONTACT FORM END--> 
    </section> 
    <!-- CONTENT START -->
<script type="text/javascript">
    $(".submitForm").on("click" , function(){
        if($(".name").val() == "" || $(".name").val() == null)
        {
            return;
        }
        if($(".message").val() == "" || $(".message").val() == null)
        {
            return;
        }
        $.post("send-message.php" , {   
            "name" : $(".name").val(),
            "email" : $(".email").val(),
            "phone" : $(".phone").val(),
            "message" : $(".message").val()
        } , function(data , status){
            if(data == 1)
            {
                $(".name").val('');
                $(".email").val('');
                $(".phone").val('');
                $(".message").val('');
                $(".alertMsg").css("display","block");
                setTimeout(function(){
                    $(".alertMsg").css("display","none");
                },3000);
            }
        }); 
    });
</script>
<?php 
  @include("footer.php");
?>