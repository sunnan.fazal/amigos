<?php 
  @include("template.php"); 
  require("conn.php");
  $sql = "SELECT * from projects";
  $query = $conn->query($sql);
?>
<div class="sections">
    <div class="container">
        <div class="pages-title">
            <h1>AMIGOS <br> <span>PROJECT GRID</span></h1>
            <p><a href="index.php">Home</a> &nbsp; > &nbsp; <a href="projects.php">PROJECTS</a></p>
        </div>
    </div>  
</div>
    <section>
        <div class="container">
          <div class="row">
              <div class="col-sm-12">
                <div class="section-tittle-alt">
                <h5>OUR PROUD</h5>
                <h2>PROJECTS</h2>
            </div>  
              </div>
            </div>
		      <div class="grid" id="kehl-grid">
            <div class="grid-sizer"></div>
            <?php
              while($row = $query->fetch_assoc())
              {
            ?>            
                <div class="grid-box chemical">
                    <a class="image-popup-vertical-fit" href="<?= "admin/".$row["IMAGE"] ?>">
                        <div class="image-mask"></div>
                        <img src="<?= "admin/".$row["IMAGE"] ?>" style="height: 13.35em;" alt="" />
                        <h3>Chemical</h3>
                    </a>
               </div>
             <?php
              }
             ?>
          </div>	
        </div>
    </section>
<?php 
  @include("footer.php");
?>
