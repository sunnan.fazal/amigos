<?php
require("conn.php");
$sql = "SELECT * from sliders";
$query = $conn->query($sql);
?>
    <div class="home-slider">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <?php
                while ($row = $query->fetch_assoc()) {
                 ?>
                    <div class="carousel-item active" style="background-image: url(<?= "admin/".$row['IMAGE'] ?>)">
                      <div class="container">
                          <div class="slider-caption slider-caption-alt">
                              <h2 class="display-4 animated fadeInRight"><?= $row['FIRST_HEADING'] ?> <br><span><?= $row['SECOND_HEADING'] ?></span></h2>
                              <p class="lead animated fadeInRight"><?= $row['PARAGRAPH'] ?></p>
                          </div>  
                      </div>
                    </div>   
                 <?php
                }
              ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon custom-control" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon custom-control" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>  
    </div>
    <!--SLIDER END-->