<!--HEADER START-->
    <header>
        <!--TOP HEADER START-->
        <div class="top-header">
            <div class="container">
                <div class="header-left">
                    <p><strong>Mobile:</strong> +92-343-5200432</p>
                </div>
                <div class="header-right">
                   <div class="ht-right-email">
                        <p>support@amigoslpg.com</p>
                   </div>
                   <div class="ht-right-social">
                        <a href="#"><i class="fab fa-facebook-f"></i></a>    
                   </div>
                   <div class="ht-right-social">
                        <a href="#"><i class="fab fa-twitter"></i></a>
                   </div>
                   <div class="ht-right-social">
                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                   </div>
                   <div class="form-group has-search">
                    <span class="fa fa-search form-control-feedback"></span>
                    <input type="text" class="form-control" placeholder="Search">
                  </div>
                </div>
            </div>
        </div>
        <!--TOP HEADER END-->
        
        <!--MEGAMENU START-->
        <div class="main-nav" id="navbar">
            <div class="container">
                <nav id="navigation1" class="navigation">
                    <div class="nav-header">
                        <a class="nav-logo" href="index.html">
                            <img src="img/master/logo.png"  class="white-logo" alt="">
                        </a>
                        <div class="nav-toggle"></div>
                    </div>
                    <div class="nav-menus-wrapper">
                        <ul class="nav-menu align-to-right">
                            <li><a href="index.php">HOME</a>
                            <li><a href="gallery.php">GALLERY</a></li>
                            <li><a href="projects.php">PROJECTS</a></li>
                            <li><a href="tenders.php">TENDERS</a></li>
                            <li><a href="career.php">CAREERS</a></li>
                            <li><a href="contact.php">CONTACT US</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--MEGAMENU END-->         
    </header>
    <!--HEADER END-->
