<?php 
  @include("template.php"); 
  require("conn.php");
  $sql = "SELECT * from tender";
  $query = $conn->query($sql);
?>
<div class="sections">
    <div class="container">
        <div class="pages-title">
            <h1>AMIGOS <br> <span>TENDERS</span></h1>
            <p><a href="index.php">Home</a> &nbsp; > &nbsp; <a href="projects.php">TENDERS</a></p>
        </div>
    </div>  
</div>
    <section>
        <div class="container">
          <div class="row">
              <div class="col-sm-12">
                <div class="section-tittle-alt">
                <h5>OPEN</h5>
                <h2>TENDERS</h2>
            </div>  
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <?php
                  while($row = $query->fetch_assoc())
                  {
                    echo $row["DESCRIPTION"];
                  }
                ?>
              </div>
            </div>
		      	
        </div>
    </section>
<?php 
  @include("footer.php");
?>
