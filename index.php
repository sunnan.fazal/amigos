<?php 
@include("template.php"); 
@include("slider.php");
@include("services.php");
require("conn.php");
$sql = "SELECT * from projects";
$query = $conn->query($sql);
?>
<!-- CONTENT START -->
    <section>
        <!-- ABOUT START -->
        <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <div class="og-about">
                    <h5>ABOUT AMIGOS LPG</h5>
                    <h2>Creating quality urban lifestyles, building stronger communities.</h2> 
                    <figure class="signature"><img src="img/images/signature.png" alt=""></figure>
                </div>  
              </div>
              <div class="col-lg-6">
                <div class="og-info">
                    <p>AMIGOS LPG has met the demands of a growing world. The farmer has a <strong>tremendous opportunity to answer the call of agricultural needs across the globe</strong>. Has 26 affiliated state soybean associations representing 30 soybean-producing state. </p>
                </div>
              </div>
            </div>
            <hr class="section-divider">
            <div class="about-bar">
                <div class="row">
                  <div class="col-lg-4">
                    <div class="ab-box">
                        <figure class="ab-icon"><img src="img/master/engineer.svg" alt=""></figure>  
                        <div class="ab-caption">
                            <h4>Qualified Engineers</h4>
                        </div>
                    </div>
                  </div>
                  <div class="col-lg-4 center-box">
                    <div class="ab-box">
                        <figure class="ab-icon"><img src="img/master/shield.svg" alt=""></figure>  
                        <div class="ab-caption">
                            <h4>Extended Warranty</h4>
                        </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="ab-box">
                        <figure class="ab-icon"><img src="img/master/innovation.svg" alt=""></figure>  
                        <div class="ab-caption">
                            <h4>Innovation</h4>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        
        <!-- WIDE SECTION START -->
        <div class="container-wide-grid">
            <div class="row">
              <div class="col-lg-6 full-thumb-layer"></div>
              <div class="col-lg-6 full-caption-layer">
                <div class="fc-content">
                  <h5>Oil and Gas</h5>
                  <h2>Fueling <span>The Future</span></h2>
                  <p>For almost 100 years, AMIGOS LPG has met the demands of a growing world. The AMIGOS LPG farmer has a tremendous opportunity to answer the call of agricultural needs across the globe. Has 26 affiliated state soybean associations representing 30 soybean-producing states and more than 300,000 U.S. soybean farmers.</p>
                  <div class="span-checklist">
                    <p>The most qualified Engineers.</p>
                    <p>We provide the most complete warranty.</p>
                    <p>Care about continuous innovation.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="order-last order-md-12 col-lg-6 full-caption-layer">
                    <div class="fc-content fc-left-align">
                      <h5>Petroleum</h5>
                      <h2>Powering <span>Progress</span></h2>
                      <p>For almost 100 years, AMIGOS LPG has met the demands of a growing world. The AMIGOS LPG farmer has a tremendous opportunity to answer the call of agricultural needs across the globe. Has 26 affiliated state soybean associations representing 30 soybean-producing states and more than 300,000 U.S. soybean farmers.</p>
                      <div class="span-checklist">
                        <p>Independent from Potential to Production.</p>
                        <p>Power for a cleaner world.</p>
                        <p>The less you burn the more you earn.</p>
                      </div>
                    </div>
                  </div>
                  <div class="order-first order-md-12 col-lg-6 full-thumb-layer-alt"></div>
            </div>
        </div>
        <!-- WIDE SECTION END -->
        
        <div class="project-grid-bg">
            <!-- PROJECT GRID START -->
            <div class="container">
                <div class="section-title">
                    <h2>Recent Projects</h2>
                    <p>The AMIGOS LPG has met the demands of a growing world. The AMIGOS LPG farmer has a tremendous opportunity to answer the call.</p>
                </div>
                <!-- <div class="filter-container">
                    <ul class="filter">
                        <li class="active" data-filter="*">All</li>
                        <li data-filter=".gas">Gas</li>
                        <li data-filter=".petroleum">Petroleum</li>
                        <li data-filter=".oil">Oil</li>
                    </ul>
                </div> -->
                <div class="grid" id="kehl-grid">
                    <div class="grid-sizer"></div>            
                    <?php
                    while ($row = $query->fetch_assoc()) {
                    ?>
                        <div class="grid-box gas">
                            <a class="image-popup-vertical-fit" href="<?= "admin/".$row["IMAGE"] ?>">
                                <div class="image-mask"></div>
                                <img src="<?= "admin/".$row["IMAGE"] ?>" style="height: 9.4em;" alt="" />
                                <h3><?= $row["PROJECT_TITLE"] ?></h3>
                            </a>
                        </div>
                    <?php
                    }  
                    ?>
                </div>
            </div>
            <!-- PROJECT GRID END -->
        </div>
        
        <!-- ACCORDION SECTION START -->
        <div class="container">
            <div class="row">
              <div class="col-lg-7">
                <div class="og-section-tittle">
                  <h5>Cleaner World</h5>
                  <h2>Powerful For Solutions</h2>
                </div> 
                <div class="og-about-alt">
                    <p><strong>For almost 100 years, AMIGOS LPG has met the demands of a growing world. The AMIGOS LPG farmer has a tremendous opportunity to answer the call of agricultural.</strong></p>
                    <p>A primary focus of AMIGOS LPG is policy development and implementation. Policy development starts with our farmer/members and culminates at the annual meeting of voting delegates. I s tasked with accomplishing the policy goals established.</p>  
                    <div class="og-accordion">
                        <ul class="accordion">
                            <li>
                                <a>How can i contact you?</a>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its grid-item. The point of using Lorem Ipsum.</p>
                            </li>
                            <li>
                                <a>What is your payment method?</a>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its grid-item. The point of using Lorem Ipsum.</p>
                            </li>
                            <li class="last-item">
                                <a>What are gas solutions?</a>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its grid-item. The point of using Lorem Ipsum.</p>
                            </li>
                        </ul> <!-- / accordion -->
                    </div>
                </div>
              </div>
              <div class="col-lg-5 space-break">
                <figure class="worker-portrait">
                    <img src="img/images/woman-worker.jpg" alt="">
                </figure>  
              </div>
            </div>
        </div>
        <!-- ACCORDION SECTION END -->
        
<!-- BUSINESS START -->
       <!--  <div class="container">
            <div class="section-title">
                <h2>OUR BUSINESSES</h2>
                <p>We are helping world in multiple ways</p>
            </div>
            <div class="news-carousel slider hover-effects image-hover">
                  <div class="blog-preview slide">
                    <div class="thumbnail-box">
                        <figure class="bp-thumbnail"><a href="#"><img src="img/images/post-preview1.jpg" alt=""></a></figure> 
                        <div class="post-date">
                            <p>22<br><span>June</span></p>
                        </div>
                    </div> 
                    <div class="bp-caption">
                        <h3>The Industry Evolution</h3>
                        <div class="social-interaction">
                            <div class="si-author"><p>Bob Kucera</p></div>
                            <div class="si-comments"><p>312 Comments</p></div>
                        </div>
                        <p>The AMIGOS LPG farmer has a tremendous opportunity to answer the call of agricultural needs across.</p>
                        <h5><a href="#">Read More</a></h5>
                    </div>
                </div>
                  <div class="blog-preview slide">
                    <div class="thumbnail-box">
                        <figure class="bp-thumbnail"><a href="#"><img src="img/images/post-preview2.jpg" alt=""></a></figure> 
                        <div class="post-date">
                            <p>18<br><span>June</span></p>
                        </div>
                    </div> 
                    <div class="bp-caption">
                        <h3>Advances In The Mechanics</h3>
                        <div class="social-interaction">
                            <div class="si-author"><p>Nick Doe</p></div>
                            <div class="si-comments"><p>116 Comments</p></div>
                        </div>
                        <p>Tasked with accomplishing the policy goals established by the farmers before Congress.</p>
                        <h5><a href="#">Read More</a></h5>
                    </div>
                </div>
                  <div class="blog-preview slide">
                    <div class="thumbnail-box">
                        <figure class="bp-thumbnail"><a href="#"><img src="img/images/post-preview3.jpg" alt=""></a></figure> 
                        <div class="post-date">
                            <p>14<br><span>May</span></p>
                        </div>
                    </div> 
                    <div class="bp-caption">
                        <h3>Roth Srategy We Wish</h3>
                        <div class="social-interaction">
                            <div class="si-author"><p>John Wilson</p></div>
                            <div class="si-comments"><p>433 Comments</p></div>
                        </div>
                        <p>This legislative process cannot happen without member input and support relevant audiences.</p>
                        <h5><a href="#">Read More</a></h5>
                    </div>
                </div>
                  <div class="blog-preview slide">
                    <div class="thumbnail-box">
                        <figure class="bp-thumbnail"><a href="#"><img src="img/images/post-preview4.jpg" alt=""></a></figure> 
                        <div class="post-date">
                            <p>12<br><span>May</span></p>
                        </div>
                    </div> 
                    <div class="bp-caption">
                        <h3>Houses Building Increased</h3>
                        <div class="social-interaction">
                            <div class="si-author"><p>Sam Jones</p></div>
                            <div class="si-comments"><p>205 Comments</p></div>
                        </div>
                        <p>This legislative process cannot happen without member input and support relevant audiences.</p>
                        <h5><a href="#">Read More</a></h5>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- BUSINESS END -->


        <!-- LATEST NEWS START -->
      <!--   <div class="container">
            <div class="section-title">
                <h2>Latest News</h2>
                <p>We specialise in intelligent & effective Search and believes in the power of partnerships to grow business.</p>
            </div>
            <div class="news-carousel slider hover-effects image-hover">
              <div class="slide">
                  <div class="blog-preview">
                    <div class="thumbnail-box">
                        <figure class="bp-thumbnail"><a href="#"><img src="img/images/post-preview1.jpg" alt=""></a></figure> 
                        <div class="post-date">
                            <p>22<br><span>June</span></p>
                        </div>
                    </div> 
                    <div class="bp-caption">
                        <h3>The Industry Evolution</h3>
                        <div class="social-interaction">
                            <div class="si-author"><p>Bob Kucera</p></div>
                            <div class="si-comments"><p>312 Comments</p></div>
                        </div>
                        <p>The AMIGOS LPG farmer has a tremendous opportunity to answer the call of agricultural needs across.</p>
                        <h5><a href="#">Read More</a></h5>
                    </div>
                </div>
              </div>
              <div class="slide">
                  <div class="blog-preview">
                    <div class="thumbnail-box">
                        <figure class="bp-thumbnail"><a href="#"><img src="img/images/post-preview2.jpg" alt=""></a></figure> 
                        <div class="post-date">
                            <p>18<br><span>June</span></p>
                        </div>
                    </div> 
                    <div class="bp-caption">
                        <h3>Advances In The Mechanics</h3>
                        <div class="social-interaction">
                            <div class="si-author"><p>Nick Doe</p></div>
                            <div class="si-comments"><p>116 Comments</p></div>
                        </div>
                        <p>Tasked with accomplishing the policy goals established by the farmers before Congress.</p>
                        <h5><a href="#">Read More</a></h5>
                    </div>
                </div>
              </div>
              <div class="slide">
                  <div class="blog-preview">
                    <div class="thumbnail-box">
                        <figure class="bp-thumbnail"><a href="#"><img src="img/images/post-preview3.jpg" alt=""></a></figure> 
                        <div class="post-date">
                            <p>14<br><span>May</span></p>
                        </div>
                    </div> 
                    <div class="bp-caption">
                        <h3>Roth Srategy We Wish</h3>
                        <div class="social-interaction">
                            <div class="si-author"><p>John Wilson</p></div>
                            <div class="si-comments"><p>433 Comments</p></div>
                        </div>
                        <p>This legislative process cannot happen without member input and support relevant audiences.</p>
                        <h5><a href="#">Read More</a></h5>
                    </div>
                </div>
              </div>
              <div class="slide">
                  <div class="blog-preview">
                    <div class="thumbnail-box">
                        <figure class="bp-thumbnail"><a href="#"><img src="img/images/post-preview4.jpg" alt=""></a></figure> 
                        <div class="post-date">
                            <p>12<br><span>May</span></p>
                        </div>
                    </div> 
                    <div class="bp-caption">
                        <h3>Houses Building Increased</h3>
                        <div class="social-interaction">
                            <div class="si-author"><p>Sam Jones</p></div>
                            <div class="si-comments"><p>205 Comments</p></div>
                        </div>
                        <p>This legislative process cannot happen without member input and support relevant audiences.</p>
                        <h5><a href="#">Read More</a></h5>
                    </div>
                </div>
              </div>
            </div>
        </div> -->
        <!-- LATEST NEWS END -->
    </section> 
    <!-- CONTENT START -->
    <?php 
      @include("footer.php");
    ?>