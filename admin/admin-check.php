<?php
session_start();
require("../conn.php");    
 if ($_POST['username'] && $_POST['password']) {

	$string = "SELECT * FROM ADMIN_USERS";
	$result = $conn->query($string);
	if ($result->num_rows > 0) {
		$enter = false;
	    while($row = $result->fetch_assoc()) {
	        if ($row['USERNAME'] == $_POST['username'] && $row['PASS'] == $_POST['password']) {
	        		$_SESSION['user'] =  array('id' => $row['ID'] , 'username' => $row['USERNAME'] );
	        		header("Location: dashboard.php");
	        		$enter = true;
	        }
	    }
	    if (! $enter) {
		header("Location: index.php");
	    }
	} else {
		header("Location: index.php");
	}		
}
else
{
	header("Location: index.php");
}

?>