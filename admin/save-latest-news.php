<?php
ob_clean();
session_start();
require("../conn.php");
if (!isset($_SESSION['user']["id"]))
{
    header("Location: index.php");
}
if(isset($_FILES['image']))
{
    $target_dir = "../latest-news/";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check !== false) {
        //
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
// Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
            $news = $_POST['news'];
            $sql  = "INSERT INTO latest_news(NEWS_TITLE , NEWS_IMG) VALUES ('".$news."' , '".$target_file."')";
            if($conn->query($sql) > 0)
            {
                header("Location: latest-news.php");
            }
            else{
                echo 'Something Went Wrong !';
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}
?>
