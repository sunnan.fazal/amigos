<!DOCTYPE html>
<html>
<head>
    <script>
        UPLOADCARE_PUBLIC_KEY = '4fbb1f8724f889b75f00';
    </script>
        <meta charset="UTF-8">
        <title>AMIGOS LPG Admin Panel</title>   
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="description" content="">
        <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">
        <meta name="author" content="Huban Creative">

        <!-- Base Css Files -->
        <link href="assets/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
        <link href="assets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/libs/fontello/css/fontello.css" rel="stylesheet" />
        <link href="assets/libs/animate-css/animate.min.css" rel="stylesheet" />
        <link href="assets/libs/nifty-modal/css/component.css" rel="stylesheet" />
        <link href="assets/libs/magnific-popup/magnific-popup.css" rel="stylesheet" /> 
        <link href="assets/libs/ios7-switch/ios7-switch.css" rel="stylesheet" /> 
        <link href="assets/libs/pace/pace.css" rel="stylesheet" />
        <link href="assets/libs/sortable/sortable-theme-bootstrap.css" rel="stylesheet" />
        <link href="assets/libs/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
        <link href="assets/libs/jquery-icheck/skins/all.css" rel="stylesheet" />
        <!-- Code Highlighter for Demo -->
        <link href="assets/libs/prettify/github.css" rel="stylesheet" />
        
                <!-- Extra CSS Libraries Start -->
                <link href="assets/libs/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
                <link href="assets/libs/morrischart/morris.css" rel="stylesheet" type="text/css" />
                <link href="assets/libs/jquery-jvectormap/css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
                <link href="assets/libs/jquery-clock/clock.css" rel="stylesheet" type="text/css" />
                <link href="assets/libs/bootstrap-calendar/css/bic_calendar.css" rel="stylesheet" type="text/css" />
                <link href="assets/libs/sortable/sortable-theme-bootstrap.css" rel="stylesheet" type="text/css" />
                <link href="assets/libs/jquery-weather/simpleweather.css" rel="stylesheet" type="text/css" />
                <link href="assets/libs/bootstrap-xeditable/css/bootstrap-editable.css" rel="stylesheet" type="text/css" />
                <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
                <!-- Extra CSS Libraries End -->
        <link href="assets/css/style-responsive.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <link rel="shortcut icon" href="assets/img/favicon.ico">
        <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="assets/img/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="assets/img/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="assets/img/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="assets/img/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="assets/img/apple-touch-icon-152x152.png" />
    </head>
    <body class="fixed-left">

    <div id="wrapper">

        <!-- Top Bar Start -->
        <div class="topbar">
            <div class="topbar-left">
                <div class="logo">
                    <center><h3><a href=""><?= $_SESSION["user"]["name"] ?></a></h3></center>
                </div>
                <button class="button-menu-mobile open-left">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
        </div>
        <!-- Top Bar End -->
        <!-- Left Sidebar Start -->
        <div class="left side-menu">
            <div class="sidebar-inner slimscrollleft">
                <div class="clearfix"></div>
                <!--- Profile -->
                <!--- Divider -->
                <div class="clearfix"></div>
                <hr class="divider" />
                <div class="clearfix"></div>
                <!--- Divider -->
                <div id="sidebar-menu">
                    <ul>
                        <li class='has_sub'>
                            <a href='javascript:void(0);'>
                                <i class='icon-home-3'></i><span>Home Page</span> <span class="pull-right">
                            <i class="fa fa-angle-down"></i></span></a>
                            <ul>
                                <li><a href='all-sliders.php'><span>Sliders</span></a></li>
                                <li><a href='all-services.php'><span>Services</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li class='has_sub'>
                            <a href='javascript:void(0);'>
                                <i class='icon-home-3'></i><span>Projects</span> <span class="pull-right">
                            <i class="fa fa-angle-down"></i></span></a>
                            <ul>
                                <li><a href='all-projects.php'><span>All Projects</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- <ul>
                        <li class='has_sub'>
                            <a href='javascript:void(0);'>
                                <i class='icon-home-3'></i><span>Businesses</span> <span class="pull-right">
                            <i class="fa fa-angle-down"></i></span></a>
                            <ul>
                                <li><a href='mission-statement.php'><span>All Businesses</span></a></li>
                            </ul>
                        </li>
                    </ul> -->
                    <!-- <ul>
                        <li class='has_sub'>
                            <a href='javascript:void(0);'>
                                <i class='icon-home-3'></i><span>News</span> <span class="pull-right">
                            <i class="fa fa-angle-down"></i></span></a>
                            <ul>
                                <li><a href='organizational-structure.php'><span>News & Achievments</span></a></li>
                            </ul>
                        </li>
                    </ul> -->
                    <ul>
                        <li class='has_sub'>
                            <a href='javascript:void(0);'>
                                <i class='icon-home-3'></i><span>Gallery</span> <span class="pull-right">
                            <i class="fa fa-angle-down"></i></span></a>
                            <ul>
                                <li><a href='all-gallery.php'><span>Gallery</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li class='has_sub'>
                            <a href='javascript:void(0);'>
                                <i class='icon-home-3'></i><span>Tenders</span> <span class="pull-right">
                            <i class="fa fa-angle-down"></i></span></a>
                            <ul>
                                <li><a href='tenders.php'><span>Tenders Section</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li class='has_sub'>
                            <a href='javascript:void(0);'>
                                <i class='icon-home-3'></i><span>Careers</span> <span class="pull-right">
                            <i class="fa fa-angle-down"></i></span></a>
                            <ul>
                                <li><a href='careers.php'><span>Career Section</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li class='has_sub'>
                            <a href='javascript:void(0);'>
                                <i class='icon-home-3'></i><span>Contact</span> <span class="pull-right">
                            <i class="fa fa-angle-down"></i></span></a>
                            <ul>
                                <li><a href='all-messages.php'><span>All Messages</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="clearfix"></div><br><br><br>
            </div>
        </div>
        <!-- Right Sidebar End -->
        <!-- Start right content -->
        <div class="content-page">
            <!-- Start Content here -->
            <!-- ============================================================== -->
