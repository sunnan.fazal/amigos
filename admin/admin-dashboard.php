<?php
require("admin-header-files.php");
if (!isset($_SESSION['user']["id"]))
    {
        header("Location: index.php");
    }
    require("dashboard-content.php");
    require("admin-footer-files.php");
?>