<?php
ob_clean();
session_start();
require("admin-header-files.php");
require("../conn.php");

if (!isset($_SESSION['user']["id"]))
{
    header("Location: index.php");
}
?>

<br><br>
<div class="container">
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content">
        <!-- Page Heading Start -->
        <div class="page-heading">
            <h1><i class='fa fa-table'></i> Slider Images</h1>
            <h3>Slider Images Listed Here</h3>            	</div>

        <!-- Page Heading End-->				<!-- Your awesome content goes here -->

        <div class="row">

            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header transparent">
                        <div class="additional-btn" style="float: left;right: 0px !important;left: 0px;">
                            <a href="add-slider.php" class="btn btn-success" style="color: white"> Add New Slider </a>
                        </div>
                    </div>
                    <br>
                    <div class="widget-content">
                        <div class="table table-responsive table-bordered">
                            <table data-sortable class="table">
                                <thead>
                                <tr>
                                    <th style="width:20% ;">Images</th>
                                    <th style="width:20% ;">Main Heading</th>
                                    <th style="width:20% ;">Heading</th>
                                    <th style="width:20% ;">Description</th>
                                    <th style="width:10%  " data-sortable="false">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                $sql = "SELECT * FROM sliders";
                                $query = $conn->query($sql);
                                while ($row = $query->fetch_assoc())
                                {
                                    ?>
                                    <tr>
                                        <td>
                                            <img src="<?= $row['IMAGE']?>" class="img-responsive" width="30%">
                                        </td>
                                        <td>
                                            <p><?= $row['FIRST_HEADING']?></p>
                                        </td>
                                        <td>
                                            <p><?= $row['SECOND_HEADING']?></p>
                                        </td>
                                        <td>
                                            <p><?= $row['PARAGRAPH']?></p>
                                        </td>
                                        <td>
                                            <a href="delete-slider.php?slider_id=<?= $row['ID']?>" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Start -->
        <footer>
            MEGATECH Corporation 2018
            <div class="footer-links pull-right">
                <a href="http://www.megatech.com.pk">Contact Us</a>
            </div>
        </footer>
        <!-- Footer End -->
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

</div>



<?php
require("admin-footer-files.php");
?>
