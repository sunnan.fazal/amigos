<?php
ob_clean();
session_start();
require("admin-header-files.php");
require("../conn.php");

if (!isset($_SESSION['user']["id"]))
{
    header("Location: index.php");
}
?>
<br><br>
<div class="container">
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content">
        <!-- Page Heading Start -->
        <div class="page-heading">
            <h1><i class='fa fa-table'></i> Add Project</h1>
            <h3>Add Project Here</h3>            	</div>

        <!-- Page Heading End-->				<!-- Your awesome content goes here -->

        <div class="row">

            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-content padding">
                        <div id="basic-form">
                            <form method="POST" action="save-project.php"  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="mainTitle" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>Upload Image</label>
                                    <input type="file" name="image" class="form-control" >
                                </div>
                                <input type="submit" class="btn btn-success" value="Save Project">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Start -->
        <footer>
            MEGATECH Corporation 2018
            <div class="footer-links pull-right">
                <a href="http://www.megatech.com.pk">Contact Us</a>
            </div>
        </footer>
        <!-- Footer End -->
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

</div>



<?php
require("admin-footer-files.php");
?>
