<?php
ob_clean();
session_start();
require("admin-header-files.php");
require("../conn.php");

if (!isset($_SESSION['user']["id"]))
{
    header("Location: index.php");
}
?>

<br><br>
<div class="container">
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content">
        <!-- Page Heading Start -->
        <div class="page-heading">
            <h1><i class='fa fa-table'></i> All Messages</h1>
            <h3>All Messages Listed Here</h3>            	</div>

        <!-- Page Heading End-->				<!-- Your awesome content goes here -->

        <div class="row">

            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-content">
                        <div class="table table-responsive table-bordered">
                            <table data-sortable class="table">
                                <thead>
                                <tr>
                                    <th style="width:15% ;">Name</th>
                                    <th style="width:10% ;">Email</th>
                                    <th style="width:10% ;">Phone</th>
                                    <th style="width:65% ;">Description</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                $sql = "SELECT * FROM contact";
                                $query = $conn->query($sql);
                                while ($row = $query->fetch_assoc())
                                {
                                    ?>
                                    <tr>
                                        <td>
                                            <label for=""><?= $row['NAME'] ?></label>
                                        </td>
                                        <td>
                                            <label for=""><?= $row['EMAIL'] ?></label>
                                        </td>
                                        <td>
                                            <label for=""><?= $row['PHONE'] ?></label>
                                        </td>
                                        <td>
                                            <label for=""><?= $row['MSG'] ?></label>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Start -->
        <footer>
            MEGATECH Corporation 2018
            <div class="footer-links pull-right">
                <a href="http://www.megatech.com.pk">Contact Us</a>
            </div>
        </footer>
        <!-- Footer End -->
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

</div>



<?php
require("admin-footer-files.php");
?>
