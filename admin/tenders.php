<?php
ob_clean();
session_start();
require("admin-header-files.php");
require("../conn.php");
if (!isset($_SESSION['user']["id"]))
{
    header("Location: index.php");
}
?>
<br><br>
<div class="container">
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content">
        <!-- Page Heading Start -->
        <div class="page-heading">
            <h1><i class='fa fa-table'></i> TENDERS</h1>
            <h3>Manage Your Tenders Section Here</h3>            	</div>

        <!-- Page Heading End-->				<!-- Your awesome content goes here -->
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-content padding">
                        <div id="basic-form">
                            <form method="POST" action="save-tender.php"  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Tender Details</label>
                                    <?php
                                    $content = "";
                                    $sql = "SELECT DESCRIPTION FROM tender";
                                    $query = $conn->query($sql);
                                    while ($row = $query->fetch_assoc()) {
                                        $content =  $row['DESCRIPTION'];
                                        break;
                                    }
                                    ?>
                                    <textarea name="paragraph" class="form-control" cols="30" rows="100"><?= $content ?></textarea>
                                </div>
                                <input type="submit" class="btn btn-success" value="Save Tender Section">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Start -->
        <footer>
            MEGATECH Corporation 2018
            <div class="footer-links pull-right">
                <a href="http://www.megatech.com.pk">Contact Us</a>
            </div>
        </footer>
        <!-- Footer End -->
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->
</div>
<script src="../ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace('paragraph');
</script>
<?php
require("admin-footer-files.php");
?>

