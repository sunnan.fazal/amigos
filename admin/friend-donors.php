<?php
ob_clean();
session_start();
require("admin-header-files.php");
require("../conn.php");
if (!isset($_SESSION['user']["id"]))
{
    header("Location: index.php");
}
?>
<style>

    .programm {
        table-layout: fixed;
    }
    .programm th {
        word-wrap: break-word;
    }
</style>
<br><br>
<div class="container">
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content">
        <!-- Page Heading Start -->
        <div class="page-heading">
            <h1><i class='fa fa-table'></i> Donors & Partners</h1>
            <h3>All Partners Listed Here</h3>            	</div>

        <!-- Page Heading End-->				<!-- Your awesome content goes here -->

        <div class="row">

            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header transparent">
                        <div class="additional-btn" style="float: left;right: 0px !important;left: 0px;">
                            <a href="add-donor.php" class="btn btn-success" style="color: white"> Add Donor </a>
                        </div>
                        <div class="additional-btn">
                            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                            <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                        </div>
                    </div>
                    <br>
                    <div class="widget-content">
                        <div class="table table-responsive table-bordered programm">
                            <table data-sortable class="table">
                                <thead>
                                <tr>
                                    <th style="width:20% ;">Donor</th>
                                    <th style="width:20% ;">Link</th>
                                    <th style="width:10%" data-sortable="false">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                $sql = "SELECT * FROM partners";
                                $query = $conn->query($sql);
                                while ($row = $query->fetch_assoc())
                                {
                                    ?>
                                    <tr>
                                        <td>
                                            <img src="<?= $row['PARTNER_IMAGE']?>" class="img-responsive" width="10%">
                                        </td>
                                        <td>
                                            <a href="<?=  $row['PARTNER_WEBSITE'] ?>"><?=  $row['PARTNER_WEBSITE'] ?></a>
                                        </td>
                                        <td>
                                            <a href="delete-partner.php?dp_id=<?= $row['ID']?>" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Start -->
        <footer>
            MEGATECH Corporation 2018
            <div class="footer-links pull-right">
                <a href="http://www.megatech.com.pk">Contact Us</a>
            </div>
        </footer>
        <!-- Footer End -->
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

</div>



<?php
require("admin-footer-files.php");
?>
