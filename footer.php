<!-- FOOTER START -->
    <footer>
        <div class="container">
            <div class="row">
              <div class="col-lg-3">
                <div class="footer-col">
                  <figure class="footer-logo"><img src="img/master/favicon.png" style="width: 3em;" alt=""></figure>
                  <p>We have over 5 years of experien able to help you 24 hours a day.</p>
                  <div class="footer-icon">
                    <div class="span-fi">
                        <div class="fi-fas"><i class="fas fa-map-marker"></i></div>
                        <div class="fi-caption">
                            <p>1st floor, Safiullah market, opposite police station totalai bazaar Totalai buner Khyber Pakhtunkhwa, Pakistan</p>  
                        </div>
                    </div> 
                    <div class="span-fi">
                        <div class="fi-fas"><i class="fas fa-phone"></i></div>
                        <div class="fi-caption">
                            <p>Mobile: +92-343-5200432</p>  
                        </div>
                    </div> 
                    <div class="span-fi">
                        <div class="fi-fas"><i class="fas fa-envelope"></i></div>
                        <div class="fi-caption">
                            <p>support@amigoslpg.com</p>  
                        </div>
                    </div>  
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="footer-col">
                  <h4>RECENT POSTS</h4>
                  <div class="media-post">
                      <figure class="media-thumb"><a href="#"><img src="img/images/media-thumb1.jpg" alt=""></a></figure>
                      <div class="media-caption">
                        <h5>LPG PUMP Opening</h5>
                        <p>1st DEC 2019</p>
                      </div>
                  </div>
                  <hr class="divider">  
                  <div class="media-post">
                      <figure class="media-thumb"><a href="#"><img src="img/images/blog-thumb.jpg" alt=""></a></figure>
                      <div class="media-caption">
                        <h5>Energy sector is growing</h5>
                        <p>1st July 2019</p>
                      </div>
                  </div>
                </div>
              </div>
              <!-- <div class="col-lg-3">
                <div class="footer-col">
                  <h4>TAGS</h4>
                  <div class="popular-links">
                    <ul>
                        <li><a href="about-us-1.html">About Company</a></li>
                        <li><a href="blog-grid.html">Blogs & News</a></li>
                        <li><a href="contact-us-1.html">Contact US</a></li>
                        <li><a href="services.html">Our Services</a></li>
                        <li><a href="#">Terms & Condditions</a></li>
                    </ul>  
                  </div>
                </div>
              </div> -->
              <div class="col-lg-3">
                <div class="footer-col last-col">
                  <h4>PHOTOS</h4>
                  <div class="footer-grid-box">
                    <div class="row">
                      <div class="col-4 col-sm-2 col-md-2 col-lg-4 gb-photos">
                        <a href="#"><img src="img/images/media-thumb2.jpg" alt=""></a>
                      </div>
                      <div class="col-4 col-sm-2 col-md-2 col-lg-4 gb-photos">
                        <a href="#"><img src="img/images/media-thumb3.jpg" alt=""></a>
                      </div>
                      <div class="col-4 col-sm-2 col-md-2 col-lg-4 gb-photos">
                        <a href="#"><img src="img/images/media-thumb4.jpg" alt=""></a>
                      </div>
                      <div class="col-4 col-sm-2 col-md-2 col-lg-4 gb-photos">
                        <a href="#"><img src="img/images/media-thumb5.jpg" alt=""></a>
                      </div>
                      <div class="col-4 col-sm-2 col-md-2 col-lg-4 gb-photos">
                        <a href="#"><img src="img/images/media-thumb6.jpg" alt=""></a>
                      </div>
                      <div class="col-4 col-sm-2 col-md-2 col-lg-4 gb-photos">
                        <a href="#"><img src="img/images/media-thumb7.jpg" alt=""></a>
                      </div>
                    </div>  
                  </div>
                </div>
              </div>
            </div>
            <hr class="divider">
            <div class="footer-bottom">
                <div class="fb-copyright">
                    <p>© 2019 MEGATECH Corporation, All rights reserved</p>
                </div>
                <div class="fb-social">
                    <div class="span-fb-social"><a href="#"><i class="fab fa-facebook-f"></i></a></div>
                    <div class="span-fb-social"><a href="#"><i class="fab fa-twitter"></i></a></div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER END -->

	<!--SCROLL TOP START-->
    <a href="#0" class="cd-top">Top</a>
    <!--SCROLL TOP START-->
    <?php 
      @include("footer_files.php")
    ?>