<?php
require("conn.php");
$sql = "SELECT * from services";
$query = $conn->query($sql);
?>
    <div class="grid-carousel-container-alt">
        <div class="grid-carousel-alt slider">
         <?php
                while ($row = $query->fetch_assoc()) {
                 ?>
                    <div class="slide">
                      <div class="grid-layer">
                          <figure class="gl-thumbnail" style="height: 13.35em;"><img src="<?= "admin/".$row['IMAGE']?>" alt=""></figure>
                          <div class="gl-caption">
                              <h3><?= $row['HEADING']?></h3>
                          </div>
                          <div class="gl-overlay-alt"></div>
                      </div> 
                    </div>

                <?php
                  }
                ?>
        </div>
    </div>
    <!--SERVICES SECTION END-->